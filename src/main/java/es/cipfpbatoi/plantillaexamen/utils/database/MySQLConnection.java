package es.cipfpbatoi.plantillaexamen.utils.database;

import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Service
public class MySQLConnection {

    private Connection connection;

    private String ip;

    private String username;

    private String password;

    private String database;

    public MySQLConnection(){
        this.ip = "127.0.0.1";
        this.database = "centro_deportivo";
        this.username = "batoi";
        this.password = "1234";
    }

    public MySQLConnection(String ip, String database, String username, String password) {
        this.ip = ip;
        this.database = database;
        this.username = username;
        this.password = password;
    }

    public MySQLConnection(String database, String username, String password) {
        this.ip = "127.0.0.1";
        this.database = database;
        this.username = username;
        this.password = password;
    }

    public Connection getConnection() {
        try {
            if (connection != null && connection.isValid(2)) {
                return connection;
            }
            String dbURL = String.format("jdbc:mysql://%s/%s?allowPublicKeyRetrieval=true",
                    ip, database);
            connection = DriverManager.getConnection(dbURL,username,password);
            return connection;
        } catch (SQLException exception) {
            throw new RuntimeException(exception.getMessage());
        }
    }


}
