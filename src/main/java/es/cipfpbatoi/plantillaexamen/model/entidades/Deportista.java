package es.cipfpbatoi.plantillaexamen.model.entidades;

import java.time.LocalDate;
import java.util.ArrayList;

public class Deportista {

    private String dni;

    private String nombre;

    private String apellidos;

    private LocalDate fechaNacimiento;

    private ArrayList<Inscripcion> inscripciones;

    public Deportista(String dni, String nombre, String apellidos, LocalDate fechaNacimiento) {
        this.dni = dni;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.fechaNacimiento = fechaNacimiento;
    }

    public Deportista(String dni) {
        this(dni, null, null, null);
    }

    public void setInscripciones(ArrayList<Inscripcion> inscripciones) {
        this.inscripciones = inscripciones;
    }

    public String getDni() {
        return dni;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public ArrayList<Inscripcion> getInscripciones() {
        return inscripciones;
    }
}
