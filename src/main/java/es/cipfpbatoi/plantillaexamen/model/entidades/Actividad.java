package es.cipfpbatoi.plantillaexamen.model.entidades;

public class Actividad {

    private int cod;

    private String nombre;

    private String description;

    public Actividad(int cod, String nombre, String description) {
        this.cod = cod;
        this.nombre = nombre;
        this.description = description;
    }

    public Actividad(int cod) {
        this(cod, null, null);
    }

    public int getCod() {
        return cod;
    }

    public String getNombre() {
        return nombre;
    }

}
