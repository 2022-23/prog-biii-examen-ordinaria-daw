package es.cipfpbatoi.plantillaexamen.model.entidades;

import java.time.LocalDateTime;

public class Inscripcion {

    private Deportista deportista;

    private Actividad actividad;

    private LocalDateTime inscritoEn;

    public Inscripcion(Deportista deportista, Actividad actividad, LocalDateTime inscritoEn) {
        this.deportista = deportista;
        this.actividad = actividad;
        this.inscritoEn = inscritoEn;
    }

    public Actividad getActividad() {
        return actividad;
    }

    public Deportista getDeportista() {
        return deportista;
    }

    public LocalDateTime getInscritoEn() {
        return inscritoEn;
    }

    public void setActividad(Actividad actividad) {
        this.actividad = actividad;
    }
}
