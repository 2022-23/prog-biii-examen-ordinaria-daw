package es.cipfpbatoi.plantillaexamen.model.dao;

import es.cipfpbatoi.plantillaexamen.exceptions.EntityNotFoundException;
import es.cipfpbatoi.plantillaexamen.model.entidades.Actividad;
import es.cipfpbatoi.plantillaexamen.model.entidades.Deportista;
import es.cipfpbatoi.plantillaexamen.model.exceptions.DatabaseErrorException;
import es.cipfpbatoi.plantillaexamen.utils.database.MySQLConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

@Service
public class SQLDeportistaDAO {

    private MySQLConnection mySQLConnection;

    @Autowired
    public SQLDeportistaDAO(MySQLConnection mySQLConnection) {
        this.mySQLConnection = mySQLConnection;
    }

    public Deportista findById(String dni) {
        Connection connection = mySQLConnection.getConnection();
        final String sql = "SELECT * FROM deportistas WHERE dni = ? ";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, dni);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()){
                return mapFromResulset(rs);
            }
            return null;
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    public ArrayList<Deportista> findAll() {
        Connection connection = mySQLConnection.getConnection();
        final String sql = "SELECT * FROM deportistas";
        try (Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(sql);
            ArrayList<Deportista> deportistas = new ArrayList<>();
            while (rs.next()){
                Deportista deportista = mapFromResulset(rs);
                deportistas.add(deportista);
            }
            return deportistas;
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    public ArrayList<Deportista> findByDateWithInscripcion(LocalDate fecha) {
        Connection connection = mySQLConnection.getConnection();
        final String sql = "SELECT d.* FROM deportistas d LEFT JOIN inscripciones i " +
                "ON d.dni = i.dni_deportista WHERE date(i.inscrito_en) = ? GROUP BY(d.dni)";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setDate(1, Date.valueOf(fecha));
            ResultSet rs = ps.executeQuery();
            ArrayList<Deportista> deportistas = new ArrayList<>();
            while (rs.next()){
                Deportista deportista = mapFromResulset(rs);
                deportistas.add(deportista);
            }
            return deportistas;
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    public Deportista getById(String dni) {
        Deportista deportista = findById(dni);
        if (deportista == null) {
            throw new EntityNotFoundException("deportista", dni);
        }
        return deportista;
    }

    private Deportista mapFromResulset(ResultSet ps) throws SQLException {
        String dni = ps.getString("dni");
        String nombre = ps.getString("nombre");
        String apellidos = ps.getString("apellidos");
        LocalDate fechaNacimiento = ps.getDate("fecha_nacimiento").toLocalDate();
        return new Deportista(dni, nombre, apellidos, fechaNacimiento);
    }

    public void insert(Deportista deportista) {
        String sql = "INSERT INTO deportistas (dni, nombre, fecha_nacimiento, apellidos) VALUES (?,?,?,?)";
        Connection connection =  mySQLConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setString(1, deportista.getDni());
            ps.setString(2, deportista.getNombre());
            ps.setDate(3, Date.valueOf(deportista.getFechaNacimiento()));
            ps.setString(4, deportista.getApellidos());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }
}
