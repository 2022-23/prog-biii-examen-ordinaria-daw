package es.cipfpbatoi.plantillaexamen.model.dao;

import es.cipfpbatoi.plantillaexamen.model.entidades.Actividad;
import es.cipfpbatoi.plantillaexamen.model.entidades.Deportista;
import es.cipfpbatoi.plantillaexamen.model.entidades.Inscripcion;
import es.cipfpbatoi.plantillaexamen.model.exceptions.DatabaseErrorException;
import es.cipfpbatoi.plantillaexamen.utils.database.MySQLConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;

@Service
public class SQLInscripcionesDAO {

    private MySQLConnection mySQLConnection;

    @Autowired
    public SQLInscripcionesDAO(MySQLConnection mySQLConnection) {
        this.mySQLConnection = mySQLConnection;
    }

    public ArrayList<Inscripcion> findAllByUser(String dni) {
        Connection connection = mySQLConnection.getConnection();
        String sql = "SELECT * FROM inscripciones WHERE dni_deportista = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setString(1, dni);
            ResultSet rs = ps.executeQuery();
            ArrayList<Inscripcion> inscripciones = new ArrayList<>();
            while (rs.next()) {
                Inscripcion registro = mapFromResulset(rs);
                inscripciones.add(registro);
            }
            return inscripciones;
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    public void insert(Inscripcion inscripcion) {
        String sql = "INSERT INTO inscripciones (dni_deportista, cod_actividad, inscrito_en) " +
                "VALUES (?,?,?)";
        Connection connection = mySQLConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setString(1, inscripcion.getDeportista().getDni());
            ps.setInt(2, inscripcion.getActividad().getCod());
            ps.setTimestamp(3, Timestamp.valueOf(inscripcion.getInscritoEn()));
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }
    private Inscripcion mapFromResulset(ResultSet rs) throws SQLException {
        String dniDeportista = rs.getString("dni_deportista");
        int codActividad = rs.getInt("cod_actividad");
        LocalDateTime inscritoEn = rs.getTimestamp("inscrito_en").toLocalDateTime();
        Deportista deportista = new Deportista(dniDeportista);
        Actividad actividad = new Actividad(codActividad);
        return new Inscripcion(deportista, actividad, inscritoEn);
    }

}
