package es.cipfpbatoi.plantillaexamen.model.dao;

import es.cipfpbatoi.plantillaexamen.exceptions.EntityNotFoundException;
import es.cipfpbatoi.plantillaexamen.model.entidades.Actividad;
import es.cipfpbatoi.plantillaexamen.model.entidades.Deportista;
import es.cipfpbatoi.plantillaexamen.model.exceptions.DatabaseErrorException;
import es.cipfpbatoi.plantillaexamen.utils.database.MySQLConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;

@Service
public class SQLActividadDAO {

    private MySQLConnection mySQLConnection;

    @Autowired
    public SQLActividadDAO(MySQLConnection mySQLConnection) {
        this.mySQLConnection = mySQLConnection;
    }

    public Actividad findById(int cod) {
        Connection connection = mySQLConnection.getConnection();
        final String sql = "SELECT * FROM actividades WHERE cod = ? ";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, cod);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()){
                return mapFromResulset(rs);
            }
            return null;
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    public ArrayList<Actividad> findAll() {
        Connection connection = mySQLConnection.getConnection();
        final String sql = "SELECT * FROM actividades";
        try (Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(sql);
            ArrayList<Actividad> actividades = new ArrayList<>();
            while (rs.next()){
                Actividad actividads = mapFromResulset(rs);
                actividades.add(actividads);
            }
            return actividades;
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    public Actividad getById(int cod) {
        Actividad actividad = findById(cod);
        if (actividad == null) {
            throw new EntityNotFoundException("actividad", cod);
        }
        return actividad;
    }

    private Actividad mapFromResulset(ResultSet ps) throws SQLException {
        int cod = ps.getInt("cod");
        String nombre = ps.getString("nombre");
        String descripcion = ps.getString("descripcion");
        return new Actividad(cod, nombre, descripcion);
    }

}
