package es.cipfpbatoi.plantillaexamen.model.repository;

import es.cipfpbatoi.plantillaexamen.model.dao.SQLActividadDAO;
import es.cipfpbatoi.plantillaexamen.model.dao.SQLDeportistaDAO;
import es.cipfpbatoi.plantillaexamen.model.dao.SQLInscripcionesDAO;
import es.cipfpbatoi.plantillaexamen.model.entidades.Actividad;
import es.cipfpbatoi.plantillaexamen.model.entidades.Deportista;
import es.cipfpbatoi.plantillaexamen.model.entidades.Inscripcion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ActividadesRepositorio {

    private SQLActividadDAO sqlActividadDAO;

    @Autowired
    public ActividadesRepositorio(SQLActividadDAO sqlActividadDAO) {
        this.sqlActividadDAO = sqlActividadDAO;
    }

    public ArrayList<Actividad> findAll() {
        return sqlActividadDAO.findAll();
    }

}
