package es.cipfpbatoi.plantillaexamen.model.repository;

import es.cipfpbatoi.plantillaexamen.exceptions.EntityNotFoundException;
import es.cipfpbatoi.plantillaexamen.model.dao.SQLActividadDAO;
import es.cipfpbatoi.plantillaexamen.model.dao.SQLDeportistaDAO;
import es.cipfpbatoi.plantillaexamen.model.dao.SQLInscripcionesDAO;
import es.cipfpbatoi.plantillaexamen.model.entidades.Actividad;
import es.cipfpbatoi.plantillaexamen.model.entidades.Deportista;
import es.cipfpbatoi.plantillaexamen.model.entidades.Inscripcion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;

@Service
public class DeportistasRepositorio {

    private SQLInscripcionesDAO sqlInscripcionesDAO;

    private SQLDeportistaDAO sqlDeportistasDAO;

    private SQLActividadDAO sqlActividadDAO;

    @Autowired
    public DeportistasRepositorio(SQLInscripcionesDAO sqlInscripcionesDAO,
                                  SQLDeportistaDAO sqlDeportistasDAO,
                                  SQLActividadDAO sqlActividadDAO) {
        this.sqlInscripcionesDAO = sqlInscripcionesDAO;
        this.sqlDeportistasDAO = sqlDeportistasDAO;
        this.sqlActividadDAO = sqlActividadDAO;
    }

    public Deportista findByIdWithInscripciones(String dni) {
        Deportista deportista = sqlDeportistasDAO.findById(dni);
        if (deportista != null) {
            setInscripciones(deportista);
        }
        return deportista;
    }

    public Deportista getByIdWithInscripciones(String dni) {
        Deportista deportista = sqlDeportistasDAO.getById(dni);
        setInscripciones(deportista);
        return deportista;
    }

    public ArrayList<Deportista> findByDateWithInscripciones(LocalDate localDate) {
        ArrayList<Deportista> deportistas = sqlDeportistasDAO.findByDateWithInscripcion(localDate);
        for (Deportista deportista: deportistas) {
            setInscripciones(deportista);
        }
        return deportistas;
    }

    public ArrayList<Deportista> findAll() {
        return sqlDeportistasDAO.findAll();
    }

    public void save(Deportista deportista) {
        sqlDeportistasDAO.insert(deportista);
    }

    public void saveInscripcion(Inscripcion inscripcion) {
        sqlInscripcionesDAO.insert(inscripcion);
    }

    private void setInscripciones(Deportista deportista) {
        ArrayList<Inscripcion> inscripciones = sqlInscripcionesDAO.findAllByUser(deportista.getDni());
        for (Inscripcion inscripcion: inscripciones) {
            Actividad actividad = sqlActividadDAO.getById(inscripcion.getActividad().getCod());
            inscripcion.setActividad(actividad);
        }
        deportista.setInscripciones(inscripciones);
    }
}
