package es.cipfpbatoi.plantillaexamen.exceptions;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String nombreEntidad, int cod){
        this(nombreEntidad, Integer.toString(cod));
    }

    public EntityNotFoundException(String nombreEntidad, String cod){
        super(String.format("No existe ninguna entidad de tipo %s con codigo %s", nombreEntidad, cod));
    }
}
