package es.cipfpbatoi.plantillaexamen.exceptions;

public class InvalidParametersException extends RuntimeException {

    public InvalidParametersException(String fecha){
        super(String.format(" (%s) - El formato introducido es invalido debe cumplir dd/MM/yyyy",fecha));
    }

}
