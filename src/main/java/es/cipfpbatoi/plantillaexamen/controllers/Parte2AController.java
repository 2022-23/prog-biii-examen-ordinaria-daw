package es.cipfpbatoi.plantillaexamen.controllers;

import es.cipfpbatoi.plantillaexamen.model.entidades.Deportista;
import es.cipfpbatoi.plantillaexamen.model.repository.DeportistasRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;

@Controller
public class Parte2AController {

    @Autowired
    private DeportistasRepositorio deportistasRepositorio;

    @GetMapping("/add-deportista")
    public String getAddAction() {
        return "add_view";
    }

    @PostMapping("/add-deportista")
    public String postAddAction(@RequestParam String dni,
                                @RequestParam String nombre,
                                @RequestParam String apellidos,
                                @RequestParam LocalDate fechaNacimiento, RedirectAttributes redirectAttributes) {
        Deportista deportista = new Deportista(dni, nombre, apellidos, fechaNacimiento);
        deportistasRepositorio.save(deportista);
        redirectAttributes.addAttribute("dni", dni);
        return "redirect:/deportista";
    }

}
