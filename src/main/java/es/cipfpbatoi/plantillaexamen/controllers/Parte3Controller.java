package es.cipfpbatoi.plantillaexamen.controllers;

import es.cipfpbatoi.plantillaexamen.exceptions.InvalidParametersException;
import es.cipfpbatoi.plantillaexamen.model.entidades.Actividad;
import es.cipfpbatoi.plantillaexamen.model.entidades.Deportista;
import es.cipfpbatoi.plantillaexamen.model.entidades.Inscripcion;
import es.cipfpbatoi.plantillaexamen.model.repository.ActividadesRepositorio;
import es.cipfpbatoi.plantillaexamen.model.repository.DeportistasRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.HashMap;

@Controller
public class Parte3Controller {

    @Autowired
    private DeportistasRepositorio deportistasRepositorio;

    @GetMapping("/generarInforme")
    @ResponseBody
    public String getAddAction(@RequestParam String fecha) {
        try {
            LocalDate fechaLocalDate = LocalDate.parse(fecha, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
            ArrayList<Deportista> deportistas = deportistasRepositorio.findByDateWithInscripciones(fechaLocalDate);
            escribirInforme(deportistas, fechaLocalDate);
            return "informe generado correctamente encontrados " + deportistas.size() + " deportistas";
        } catch (DateTimeParseException e) {
            throw new InvalidParametersException(fecha);
        }
    }

    private void escribirInforme(ArrayList<Deportista> deportistas, LocalDate fecha) {

        try (
                FileWriter fileWriter = new FileWriter(getClass().getResource("/database/inscripciones.txt").getFile());
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        ) {
            bufferedWriter.newLine();
            bufferedWriter.write(String.format("Inscripciones realizadas en fecha %s", fecha.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy"))));
            for (Deportista deportista: deportistas) {
                bufferedWriter.newLine();
                bufferedWriter.newLine();
                bufferedWriter.write(String.format("Deportista %s %s", deportista.getNombre(), deportista.getApellidos()));
                for (Inscripcion inscripcion: deportista.getInscripciones()) {
                    bufferedWriter.newLine();
                    bufferedWriter.write(String.format("Inscrito en %s a las %dh", inscripcion.getActividad().getNombre(), inscripcion.getInscritoEn().getHour()));
                }
            }
            bufferedWriter.newLine();
            bufferedWriter.write(String.format("Busqueda realizada el %s", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy 'a las' HH:mm"))));
            bufferedWriter.newLine();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }



}
