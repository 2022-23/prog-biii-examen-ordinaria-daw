package es.cipfpbatoi.plantillaexamen.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ExamenController {

    @GetMapping("/")
    @ResponseBody
    public String exampleAction() {
          return "Benvingut a l'últim examen de programació";
    }

}
