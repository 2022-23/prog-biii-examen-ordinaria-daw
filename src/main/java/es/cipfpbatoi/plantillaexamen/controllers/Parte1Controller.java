package es.cipfpbatoi.plantillaexamen.controllers;

import es.cipfpbatoi.plantillaexamen.model.entidades.Deportista;
import es.cipfpbatoi.plantillaexamen.model.repository.DeportistasRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Parte1Controller {

    @Autowired
    private DeportistasRepositorio deportistasRepositorio;

    @GetMapping("/deportista")
    public String exampleAction(@RequestParam String dni, Model model) {
        Deportista deportista = deportistasRepositorio.getByIdWithInscripciones(dni);
        model.addAttribute("deportista", deportista);
        return "details_view";
    }

}
