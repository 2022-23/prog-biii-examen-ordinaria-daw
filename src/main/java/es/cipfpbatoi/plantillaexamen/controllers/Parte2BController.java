package es.cipfpbatoi.plantillaexamen.controllers;

import es.cipfpbatoi.plantillaexamen.model.entidades.Actividad;
import es.cipfpbatoi.plantillaexamen.model.entidades.Deportista;
import es.cipfpbatoi.plantillaexamen.model.entidades.Inscripcion;
import es.cipfpbatoi.plantillaexamen.model.repository.ActividadesRepositorio;
import es.cipfpbatoi.plantillaexamen.model.repository.DeportistasRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;

@Controller
public class Parte2BController {

    @Autowired
    private DeportistasRepositorio deportistasRepositorio;

    @Autowired
    private ActividadesRepositorio actividadesRepositorio;

    @GetMapping("/inscripcion-add")
    public String getAddAction(Model model) {
        ArrayList<Deportista> deportistas = deportistasRepositorio.findAll();
        ArrayList<Actividad> actividades = actividadesRepositorio.findAll();
        model.addAttribute("deportistas", deportistas);
        model.addAttribute("actividades", actividades);
        return "inscripcion-add-view";
    }

    @PostMapping("/inscripcion-add")
    public String getAddAction(@RequestParam HashMap<String, String> params,
                               RedirectAttributes redirectAttributes) {
        String dniDeportista = params.get("deportista_dni");
        String codActividad = params.get("actividad_cod");
        String inscritoEn = params.get("dia") + " " + params.get("hora");
        LocalDateTime localDateTime = LocalDateTime.parse(inscritoEn,  DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        Deportista deportista = new Deportista(dniDeportista);
        Actividad actividad = new Actividad(Integer.parseInt(codActividad));
        Inscripcion inscripcion = new Inscripcion(deportista, actividad, localDateTime);
        deportistasRepositorio.saveInscripcion(inscripcion);
        redirectAttributes.addAttribute("dni", dniDeportista);
        return "redirect:/deportista";
    }



}
